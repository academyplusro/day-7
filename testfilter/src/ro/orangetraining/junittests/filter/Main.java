package ro.orangetraining.junittests.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here
        // TipData x -> { sout() }

        List<User> initialList = getUsers();
        //daca se termina in a
        if(initialList
                .stream()
                .filter(
                        u -> u.getName().endsWith("a")
                ).findAny().isPresent()){
            System.out.println("ends with a");
        }
        System.out.println(
                initialList
                        .stream()
                        .map(
                                u -> u.getName().toUpperCase()
                        ).findFirst().get()
        );

        comparatorExample();
    }
    public static List<User> getUsers() {
        List<User> list = new ArrayList<User>();
        list.add(new User(1, "Diana", 20));
        list.add(new User(2, "Joanna", 15));
        list.add(new User(3, "Mary", 25));
        list.add(new User(4, "Elisabeth", 30));
        return list;
    }
    public static void comparatorExample(){
        List<User> initialList = getUsers();
        Collections.sort(initialList, (User u1, User u2) ->
                u1.getAge() - u2.getAge()
        );

        for (User u: initialList
             ) {
            System.out.println(u.getName());
        }
    }
}
