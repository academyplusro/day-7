import java.util.ArrayList;
import java.util.List;

public class Test {
   public static void main(String[] args) {
      List<Employee> Employeelist = new ArrayList<Employee>();
      Employeelist.add(new Employee("Jon", 22, 1001)); 
      Employeelist.add(new Employee("Steve", 19, 1003)); 
      Employeelist.add(new Employee("Kevin", 23, 1005)); 
      Employeelist.add(new Employee("Ron", 20, 1010)); 
      Employeelist.add(new Employee("Lucy", 18, 1111));
      System.out.println("Before Sorting the Employee data:"); 
 
      //java 8 forEach for printing the list 
      Employeelist.forEach((s)->System.out.println(s));

      System.out.println("After Sorting the Employee data by Age:"); 

      //Lambda expression for sorting by age 
      Employeelist.sort((Employee s1, Employee s2)->s1.getAge()-s2.getAge()); 

      //java 8 forEach for printing the list
      Employeelist.forEach((s)->System.out.println(s));         

      System.out.println("After Sorting the Employee data by Name:"); 
      //Lambda expression for sorting the list by Employee name  
      
      Employeelist.sort((Employee s1, Employee s2)->s1.getName().compareTo(s2.getName())); 
      Employeelist.forEach((s)->System.out.println(s));        
      System.out.println("After Sorting the Employee data by Id:");        
      //Lambda expression for sorting the list by Employee id 
      Employeelist.sort((Employee s1, Employee s2)->s1.getId()-s2.getId()); 
      Employeelist.forEach((s)->System.out.println(s)); 
   }
}