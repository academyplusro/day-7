package tddJUnit;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConCatTest {

	@Test
	public void test() {
		MyJUnitClass junit = new MyJUnitClass();
		String result = junit.ConCat("Hi", "User");
		assertEquals("HiUser",result);
	}

}
